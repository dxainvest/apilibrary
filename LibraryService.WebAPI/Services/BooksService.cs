﻿using LibraryService.WebAPI.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LibraryService.WebAPI.Services
{
    public class BooksService
    {
        private readonly TestProjectContext _testProjectContext;

        public BooksService(TestProjectContext testProjectContext)
        {
            _testProjectContext = testProjectContext;
        }

        public IEnumerable<Book> Get(int libraryId, int[] ids)
        {
            var users = _testProjectContext.Books.Where(x => x.LibraryId == libraryId).AsQueryable();

            if (ids != null && ids.Any())
                users = users.Where(x => ids.Contains(x.Id));

            return users.ToList();
        }

        public Book Add(Book book)
        {
            _testProjectContext.Books.Add(book);

            _testProjectContext.SaveChanges();
            return book;
        }

        public Book Update(Book book)
        {
            var userForChanges = _testProjectContext.Books.Single(x => x.Id == book.Id);

            userForChanges.Name = book.Name;

            _testProjectContext.Books.Update(userForChanges);
            _testProjectContext.SaveChanges();
            return book;
        }

        public bool Delete(Book book)
        {
            _testProjectContext.Books.Remove(book);
            _testProjectContext.SaveChanges();

            return true;
        }
    }

}
